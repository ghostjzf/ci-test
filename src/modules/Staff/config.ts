export const config = [
  {
    path: "/form-tool",
    title: "Form Tool",
    exact: true,
    permission: null,
    component: "TestA"
  },
  {
    path: "/component",
    title: "常用组件",
    exact: true,
    permission: null,
    component: "TestB"
  }
];
