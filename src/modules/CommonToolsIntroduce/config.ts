export const config = [
  {
    path: "/form-tool",
    title: "Form Tool",
    exact: true,
    permission: null,
    component: "TestA"
  },
  {
    path: "/git-skill",
    title: "git的使用",
    exact: true,
    permission: null,
    component: "Git"
  }
];
