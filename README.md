架构基于`react` `mobx` [`ant-design`](https://github.com/ant-design/ant-design)

## Available Scripts

In the project directory, you can run:

### `npm start`

启动本地服务.<br>
在浏览器打开链接 [http://localhost:3000](http://localhost:3000) .

环境支持热替换.<br>

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

**See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.**

### `npm run analyze`

**Note: 分析分包文件!**

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
